#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsView>
#include <clickfilter.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QString imagePath, QString measurementCmd, QString preloadFile, QWidget *parent = nullptr);
	~MainWindow();

	void loadData();
public slots:
	void dataPoint(QPoint, int64_t);

private:
	Ui::MainWindow *ui;
	QGraphicsScene *scene;
	ClickFilter cf;

	QColor dbToColor(int64_t);
	QGraphicsItem *img;
	QString preloadFile;
};

#endif // MAINWINDOW_H
