#ifndef CLICKFILTER_H
#define CLICKFILTER_H

#include <QObject>
#include <QPoint>

class ClickFilter : public QObject
{
	Q_OBJECT
public:
	explicit ClickFilter(QString, QObject *parent = nullptr);

signals:
	void dataPoint(QPoint, int64_t);
public slots:
protected:
	 bool eventFilter(QObject *obj, QEvent *event) override;
	 QString measurementCmd;
};

#endif // CLICKFILTER_H
