#include "clickfilter.h"
#include <QMouseEvent>
#include <QDebug>
#include <QProcess>
ClickFilter::ClickFilter(QString measurementCmd, QObject *parent) : QObject(parent)
{
	this->measurementCmd = measurementCmd;
}

bool ClickFilter::eventFilter(QObject *obj, QEvent *event)
{
	if (event->type() == QEvent::MouseButtonDblClick) {
			QMouseEvent *mevent = static_cast<QMouseEvent *>(event);
			QProcess myProcess(this);
			myProcess.start(this->measurementCmd, {QString::number(mevent->x()), QString::number(mevent->y())});
			myProcess.waitForFinished();
			auto ba = myProcess.readAllStandardOutput();
			qWarning() << ba << ba.toInt();
			emit dataPoint(QPoint(mevent->x(), mevent->y()), ba.toInt());
			return true;
		} else {
			// standard event processing
			return QObject::eventFilter(obj, event);
		}
}
