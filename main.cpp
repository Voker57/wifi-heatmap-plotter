#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	auto args = a.arguments();
	QString imagePath = args.at(1);
	QString measurementCmd = args.at(2);
	QString preloadFile;
	if(args.size() == 4) {
		preloadFile = args.at(3);
	}
	MainWindow w(imagePath, measurementCmd, preloadFile);
	w.show();

	return a.exec();
}
