#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QGraphicsPixmapItem>
#include <QDebug>
#include <QPoint>
#include <QFile>

MainWindow::MainWindow(QString imagePath, QString measurementCmd, QString preloadFile, QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow), cf(measurementCmd, this)
{
	ui->setupUi(this);
	this->scene = new QGraphicsScene();
	ui->graphicsView->setScene(this->scene);
	auto image = QPixmap(imagePath);
	this->img = scene->addPixmap(image);
	QObject::connect(&cf, SIGNAL(dataPoint(QPoint, int64_t)), this, SLOT(dataPoint(QPoint, int64_t)));
	ui->graphicsView->installEventFilter(&cf);
	this->preloadFile = preloadFile;
/*
	qDebug() << ui->graphicsView->viewport()->width() << image.width() << ui->graphicsView->viewport()->height() << image.height();
	double xscale = static_cast<double>(ui->graphicsView->viewport()->width()) / static_cast<double>(image.width());
	double yscale = static_cast<double>(ui->graphicsView->viewport()->height()) / static_cast<double>(image.height());
	double scale = std::min(xscale,yscale);
	qDebug() << scale;
	ui->graphicsView->scale(scale, scale);
*/	//qDebug() << it->boundingRect();
	//ui->graphicsView->ftInView(it);
	//qDebug() << ui->graphicsView->get
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::dataPoint(QPoint p, int64_t dp)
{
	if(!preloadFile.isEmpty()) {
		loadData();
	}
	ui->graphicsView->fitInView(this->img, Qt::KeepAspectRatio);
	const QPen pen(Qt::white, 0);
	const QBrush brush(dbToColor(-dp));
	//qWarning() << brush;
	const QBrush tBrush(Qt::black);
	auto qp = ui->graphicsView->mapToScene(p);
	ui->graphicsView->scene()->addEllipse(qp.x() - 25, qp.y() -25, 50, 50, pen, brush);
	auto t = ui->graphicsView->scene()->addText(QString::number(dp));
	t->setPos(qp);
}

void MainWindow::loadData() {
			QFile file(preloadFile);
			preloadFile.clear();
		file.open(QIODevice::ReadOnly);
		while (!file.atEnd()) {
			QString line = file.readLine().trimmed();
			auto splt = line.split(" ");
			dataPoint(QPoint(splt.at(0).toInt(), splt.at(1).toInt()), splt.at(2).toInt());
		}
}

QColor MainWindow::dbToColor(int64_t db)
{
	double ideal = 30;
	double unacceptable = 90;
	double normalized = (static_cast<double>(db) - ideal) / (unacceptable - ideal);
	//qWarning() << unacceptable << ideal <<  db << ideal << normalized;
	if(normalized < 0) {
		normalized = 0;
	}
	if(normalized > 1) {
		normalized = 1;
	}
	if(normalized > 0.5) {
		return QColor(255, 255.0 * (1 - (normalized - 0.5) * 2.0), 0, 100);
	} else {
		qDebug() << 255.0 * normalized * 2.0;
		return QColor(255.0 * (normalized) * 2.0, 255, 100);
	}
}
